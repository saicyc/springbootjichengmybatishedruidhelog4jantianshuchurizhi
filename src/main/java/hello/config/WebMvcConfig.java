package hello.config;

import javax.annotation.Resource;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import hello.utils.FileProperties;

@Configuration
public class WebMvcConfig extends WebMvcConfigurerAdapter {
	@Resource
	private FileProperties fileProperties;
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/site/**").addResourceLocations("/site/");
		registry.addResourceHandler(fileProperties.getUploadFolderUrlPrefix()+"**").addResourceLocations("file:"+fileProperties.getUploadFolder());
	}
	@Bean
	@ConditionalOnMissingBean(CharacterEncodingFilter.class)
	public CharacterEncodingFilter characterEncodingFilter() {
		CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
	    characterEncodingFilter.setEncoding("UTF-8");
	    characterEncodingFilter.setForceEncoding(true);
		return characterEncodingFilter;
	}
}
