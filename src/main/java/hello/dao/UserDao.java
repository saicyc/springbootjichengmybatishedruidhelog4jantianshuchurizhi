package hello.dao;

import java.util.Map;

import org.apache.ibatis.annotations.Param;

import hello.to.User;
public interface UserDao {
	//查询用户信息通过id
	User findUserById(String id);

	int updateUserName(@Param("id")int id,@Param("name")String name);

	int updateUserInfo(Map<String, Object> map);

}
