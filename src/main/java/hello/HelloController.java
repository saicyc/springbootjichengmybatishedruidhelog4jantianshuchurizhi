package hello;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hello.service.TestService;

@RestController
public class HelloController {
    @Resource
    private TestService testService;
    @RequestMapping("/")
    public String index() {
    	/*testService.findUser();
    	testService.update();*/
    	testService.updateUserInfo();
        return "Greetings from Spring Boot!";
    }
    
}
