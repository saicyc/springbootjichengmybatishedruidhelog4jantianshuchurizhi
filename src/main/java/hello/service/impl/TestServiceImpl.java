package hello.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import hello.dao.UserDao;
import hello.service.TestService;
import hello.to.User;
@Service
public class TestServiceImpl implements TestService{
	private static final Log log = LogFactory.getLog(TestServiceImpl.class);
	@Resource	
    private UserDao userDao;

	@Override
	public void findUser() {
		User user = userDao.findUserById("1");
		System.out.println("ok"+user);
	}

	@Override
	@Transactional
	public void update() {
		userDao.updateUserName(1,"2");
		System.out.println("1");
		userDao.updateUserName(2,"cyc");
		System.out.println("2");
	}
	
	@Override
	@Transactional
	public void updateUserInfo(){
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("id", "1");
		map.put("name", "cyc22");
		int count = userDao.updateUserInfo(map);
		log.info("ok12345678");
		System.out.println(count);
	}
}
