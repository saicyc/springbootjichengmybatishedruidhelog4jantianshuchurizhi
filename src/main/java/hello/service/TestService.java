package hello.service;

public interface TestService {
	void findUser();
	void update();
	void updateUserInfo();
}
