package hello.utils;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
@ConfigurationProperties(prefix = "cyc.db")
public class DBProperties {                          
	    private String url;
		private String driverClassName;
		private String username;
		private String password;
		private String filters;
		private int maxActive;
		private int initialSize;
		private int maxWait;
		private int minIdle;
		private int maxIdle;
		private long timeBetweenEvictionRunsMillis;
		private int minEvictableIdleTimeMillis;
		private String validationQuery;
		private Boolean testWhileIdle;
		private Boolean testOnBorrow;
		private Boolean testOnReturn;
		private int maxOpenPreparedStatements;
		private Boolean removeAbandoned;
		private int removeAbandonedTimeout;
		private Boolean logAbandoned;
}
