package hello.utils;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
@ConfigurationProperties(prefix = "cyc.mvc")
public class FileProperties {
	private String uploadFolderUrlPrefix;
	private String uploadFolder;
}
