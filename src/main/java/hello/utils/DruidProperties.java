package hello.utils;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
@ConfigurationProperties(prefix = "cyc.druid")
public class DruidProperties {
	private String whiteList;
	private String blackList;
	private String userName;
	private String password;
	private String resetEnable;
}
